package com.sky.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：lizhijie
 * @Date：2023/7/2 19:43
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location {
    private String lng;
    private String lat;

    public String latAndlng(){
        return lat+","+lng;
    }
}
