package com.sky.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author：lizhijie
 * @Date：2023/7/2 19:28
 * @Description:
 */
@Data
@Component
@ConfigurationProperties(prefix = "sky.baidu")
public class BaiduMapProperties {
    private String ak;
    private String geoCoderUrl;
    private String drivingUrl;
}
