package com.sky.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.exception.BusinessException;
import com.sky.properties.BaiduMapProperties;
import com.sky.result.Location;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 *@Author：lizhijie
 *@Date：2023/7/2 19:32
 *@Description:
 */
//地图工具类
@Slf4j
@AllArgsConstructor
public class MapUtils {

    private BaiduMapProperties baiduMapProperties;

    //输入地点返回经纬度坐标
    public Location countAddress(String address){
        log.info("开始计算地址{}经纬度,",address);
        Map<String,String> map = new HashMap<>();
        map.put("ak",baiduMapProperties.getAk());
        map.put("output","json");
        map.put("address",address);
        String result = HttpClientUtil.doGet(baiduMapProperties.getGeoCoderUrl(), map);
        if (!(JSON.parseObject(result).getInteger("status").equals(0))){
            log.info("解析失败");
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }
        JSONObject jsonObject = JSON.parseObject(result).getJSONObject("result").getJSONObject("location");
        return new Location(jsonObject.getString("lng"),jsonObject.getString("lat"));
    }
    public Integer countDistance(String local,String destination){
        log.info("计算两个位置距离{}和{}",local,destination);
        Map<String,String> map = new HashMap<>();
        map.put("ak",baiduMapProperties.getAk());
        map.put("destination",destination);
        map.put("origin",local);
        String result = HttpClientUtil.doGet(baiduMapProperties.getDrivingUrl(), map);
        Integer distance = JSON.parseObject(result).getJSONObject("result").getJSONArray("routes").getJSONObject(0).getInteger("distance");
        log.info("两个地点距离{}M",distance);
        return distance;
    }
}
