package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 16:06
 * @Description:
 */
public interface DishService {
    void addDish(DishDTO dishDTO);

    PageResult pageList(DishPageQueryDTO dto);

    void delete(List<Integer> ids);

    DishVO selectById(Long id);

    void update(DishDTO dishDTO);

    void dishStatus(Integer status, Long id);

    List<Dish> list(Integer categoryId, String name);

    List<DishVO> selectByCategoryId(Long id);

    List<DishItemVO> selectBySetmealId(Long id);
}
