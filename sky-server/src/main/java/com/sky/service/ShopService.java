package com.sky.service;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 15:12
 * @Description:
 */
public interface ShopService {


    Integer getStatus();

    void setStatus(Integer status);
}
