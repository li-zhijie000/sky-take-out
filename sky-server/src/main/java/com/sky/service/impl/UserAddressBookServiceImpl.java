package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.entity.AddressBook;
import com.sky.mapper.UserAddressBookMapper;
import com.sky.service.UserAddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @Author：lizhijie
 * @Date：2023/6/27 17:43
 * @Description:
 */
@Service
@Slf4j
public class UserAddressBookServiceImpl implements UserAddressBookService {
    @Autowired
    private UserAddressBookMapper userAddressBookMapper;

    /**
     * 默认地址
     * @return
     */
    @Override
    public AddressBook selectDefaultAddress() {
        AddressBook addressBook = AddressBook.builder().isDefault(1).build();
        return  userAddressBookMapper.selectById(addressBook);

    }

    /**
     * 设置默认地址
     * @param
     */
    @Override
    public void defaultAddress(AddressBook addressBook) {
        //查找是否有默认地址
        AddressBook build = AddressBook.builder().isDefault(1).build();
        AddressBook addressBook1 = userAddressBookMapper.selectById(build);
        addressBook.setIsDefault(1);
        if (Objects.isNull(addressBook1)){
            //没有默认的地址直接添加
            userAddressBookMapper.update(addressBook);
        }else {
            //有默认的地址
            addressBook1.setIsDefault(0);
            //将原本默认的地址设置为非默认
            userAddressBookMapper.update(addressBook1);
            userAddressBookMapper.update(addressBook);
        }
    }

    /**
     * 删除地址
     * @param id
     */
    @Override
    public void delete(Long id) {
        log.info("删除地址");
        userAddressBookMapper.delete(id);
    }

    /**
     * 更新地址
     * @param addressBook
     */
    @Override
    public void update(AddressBook addressBook) {
        addressBook.setUserId(BaseContext.getCurrentId());
        userAddressBookMapper.update(addressBook);
    }

    /**
     * 添加地址
     */

    @Override
    public void add(AddressBook addressBook) {
        addressBook.setUserId(BaseContext.getCurrentId());
        addressBook.setIsDefault(0);
       userAddressBookMapper.add(addressBook);
    }

    /**
     * 查询回显
     * @param id
     * @return
     */
    @Override
    public AddressBook selectById(Long id) {
        AddressBook addressBook = AddressBook.builder().id(id).build();
        addressBook=  userAddressBookMapper.selectById(addressBook);
        return addressBook;
    }


    /**
     * 展示所有地址列表
     */
    @Override
    public List<AddressBook> list() {
    List<AddressBook> addressBookList= userAddressBookMapper.list();
    return addressBookList;
    }
}
