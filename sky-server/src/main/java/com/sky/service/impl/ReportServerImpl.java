package com.sky.service.impl;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportServer;
import com.sky.vo.*;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 11:22
 * @Description:
 */
@Service
public class ReportServerImpl implements ReportServer {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HttpServletResponse response;

    /**
     * 导出Excel表格
     */
    @Override
    public void export() throws IOException {
        //获取今日时间
        LocalDate now = LocalDate.now().minusDays(1);
        LocalDate beforeNow = LocalDate.now().minusDays(31);
//        获取两个时间点
        LocalDateTime begin = LocalDateTime.of(beforeNow, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(now, LocalTime.MAX);
        //获取每日时间的集合
        List<String> dateList = beforeNow.datesUntil(now.plusDays(1)).map(date -> DateTimeFormatter.ofPattern("yyyy-MM-dd").format(date)).collect(Collectors.toList());
        //开始查询需要封装数据
        String head = beforeNow +"到"+now +"之间的运营数据展示";
        BusinessDataVO businessDataVO =  ordersMapper.selectDataByDate(begin,end);
        //查询新增的用户数量
     Integer newUserCount = CollectionUtils.isEmpty(userMapper.selectAddCount(begin, end)) ?0:userMapper.selectAddCount(begin, end).get(0).getUserCount();
       //查询有效订单数量
        List<OrderReportDTO> orderReportDTOList = ordersMapper.selectCountByDate(begin, end, Orders.ORDER_STAUTS_COMPLETED);
        Map<String, Integer> collect = orderReportDTOList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
      List<Integer> validOrderCountList =   dateList.stream().map(date->{
           return collect.get(date)==null?0:collect.get(date);
        }).collect(Collectors.toList());
        //订单数量
        List<OrderReportDTO> orderReportDTOList1 = ordersMapper.selectCountByDate(begin, end, null);
        Map<String, Integer> collect1 = orderReportDTOList1.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> allOrderCountList =   dateList.stream().map(date->{
            return collect1.get(date)==null?0:collect1.get(date);
        }).collect(Collectors.toList());
        //查询新增用户数量
        List<UserReportDTO> userReportDTOS = userMapper.selectAddCount(begin, end);
        Map<String, Integer> collect2 = userReportDTOS.stream().collect(Collectors.toMap(UserReportDTO::getCreateDate, UserReportDTO::getUserCount));
        List<Integer> NewUserList = dateList.stream().map(date -> {
            return collect2.get(date) == null ? 0 : collect2.get(date);
        }).collect(Collectors.toList());
        //查询营业额
        List<TurnoverReportDTO> turnoverReportDTOList = ordersMapper.selectTurnover(begin, end, Orders.ORDER_STAUTS_COMPLETED);
        Map<String, BigDecimal> collect3 = turnoverReportDTOList.stream().collect(Collectors.toMap(TurnoverReportDTO::getOrderDate, TurnoverReportDTO::getOrderMoney));
        List<BigDecimal> turnoverReportList = dateList.stream().map(date -> {
            return collect3.get(date) == null ? new BigDecimal(0) : collect3.get(date);
        }).collect(Collectors.toList());

        //开始向表格内进行数据的填充
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("templates/运营数据报表模板.xlsx");
        Workbook workbook = new XSSFWorkbook(resourceAsStream);
        Sheet sheet = workbook.getSheetAt(0);
        sheet.getRow(1).getCell(1).setCellValue(head);
        Row row03 = sheet.getRow(3);
        row03.getCell(2).setCellValue(businessDataVO.getTurnover());
        row03.getCell(4).setCellValue(businessDataVO.getOrderCompletionRate());
        row03.getCell(6).setCellValue(newUserCount);
        Row row4 = sheet.getRow(4);
        row4.getCell(2).setCellValue(businessDataVO.getValidOrderCount());
        row4.getCell(4).setCellValue(businessDataVO.getUnitPrice());
        //继续填充明细数据  validOrderCountList  allOrderCountList    NewUserList turnoverReportList
        for (int i = 7; i < dateList.size()+6; i++) {
            //开始取得行
            Integer index = i-7;
            Row row = sheet.getRow(i);
            row.getCell(1).setCellValue(dateList.get(index));
            row.getCell(2).setCellValue(turnoverReportList.get(index).toString());
            row.getCell(3).setCellValue(validOrderCountList.get(index));
            if (allOrderCountList.get(index)!=0){
                row.getCell(4).setCellValue(validOrderCountList.get(index).doubleValue()/allOrderCountList.get(index));
            }else {
                row.getCell(4).setCellValue(0);
            }
            if (validOrderCountList.get(index)!=0) {
                row.getCell(5).setCellValue(turnoverReportList.get(index).doubleValue()/validOrderCountList.get(index));
            }else {
                row.getCell(5).setCellValue(0);
            }
            row.getCell(6).setCellValue(NewUserList.get(index));
        }
        //开始写出
        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);
        workbook.close();
    }

    /**
     * 菜品销量排行
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO salesTop(LocalDate begin, LocalDate end) {
        LocalDateTime slectBegin = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime slectEnd = LocalDateTime.of(end, LocalTime.MAX);
       List<SalesReportDTO> salesReportDTOList= ordersMapper.selectSalesTop10(slectBegin,slectEnd,Orders.ORDER_STAUTS_COMPLETED);
         List<String> nameList=new ArrayList<>();
         List<Integer> numberList=new ArrayList<>();
        for (SalesReportDTO salesReportDTO : salesReportDTOList) {
            nameList.add(salesReportDTO.getGoodsName());
            numberList.add(salesReportDTO.getGoodsNumber());
        }
        return SalesTop10ReportVO.builder().nameList(nameList).numberList(numberList).build();
    }

    /**
     * 订单表统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public OrderReportVO ordersCount(LocalDate begin, LocalDate end) {
        //先拿取时间的集合
        List<String> dateList = begin.datesUntil(end.plusDays(1)).map(LocalDate::toString).collect(Collectors.toList());
        //将日期单独取出
        LocalDateTime slectBegin = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime slectEnd = LocalDateTime.of(end, LocalTime.MAX);
        //查询每日的订单数量
      List<OrderReportDTO> allOrder =  ordersMapper.selectCountByDate(slectBegin,slectEnd,null);
        Map<String, Integer> map = allOrder.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> orderCountList = dateList.stream().map(date -> {
            return map.get(date) == null ? 0 : map.get(date);
        }).collect(Collectors.toList());
        //查询每日的有效订单数量
        List<OrderReportDTO> orderReportDTOList = ordersMapper.selectCountByDate(slectBegin, slectEnd, Orders.ORDER_STAUTS_COMPLETED);
        Map<String, Integer> map2 = orderReportDTOList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> validOrderCountList = dateList.stream().map(date -> {
            return map2.get(date) == null ? 0 : map2.get(date);
        }).collect(Collectors.toList());
        //查询订单总数
        Integer totalOrderCount = orderCountList.stream().reduce(Integer::sum).get();
        //查询有效订单数
        Integer validOrderCount = validOrderCountList.stream().reduce(Integer::sum).get();
        //订单完成率
        Double orderCompletionRate=validOrderCount.doubleValue()/totalOrderCount;
        return new OrderReportVO(dateList,orderCountList,validOrderCountList,totalOrderCount,validOrderCount,orderCompletionRate);
    }

    /**
     * 用户数量统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public UserReportVO userCount(LocalDate begin, LocalDate end) {
        //先拿取时间的集合
        List<String> dateList = begin.datesUntil(end.plusDays(1)).map(LocalDate::toString).collect(Collectors.toList());
        //将日期单独取出
        LocalDateTime slectBegin = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime slectEnd = LocalDateTime.of(end, LocalTime.MAX);
        //进行表内查询结果
       List<UserReportDTO> userAddList =  userMapper.selectAddCount(slectBegin,slectEnd);
      //拿到每天增长数量的集合
        Map<String, Integer> map = userAddList.stream().collect(Collectors.toMap(UserReportDTO::getCreateDate, UserReportDTO::getUserCount));
        List<Integer> collect = dateList.stream().map(date -> {
            return map.get(date) == null ? 0 : map.get(date);
        }).collect(Collectors.toList());
        //拿到每天增长的数量
       Integer count =  userMapper.selectBaseCountByDate(slectBegin);
        ArrayList<Integer> userCountList = new ArrayList<>();
        for (Integer add : collect) {
            count+=add;
            userCountList.add(count);
        }
        return new UserReportVO(dateList,userCountList,collect);
    }

    /**
     * 进行一段时间营业额的查询
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO selectTurnover(LocalDate begin, LocalDate end) {
        //进行日期计算
        List<String> dateList = begin.datesUntil(end.plusDays(1)).map(LocalDate::toString).collect(Collectors.toList());
        //将日期单独取出
        LocalDateTime slectBegin = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime slectEnd = LocalDateTime.of(end, LocalTime.MAX);
        //进行表内查询结果
        List<TurnoverReportDTO> turnoverReportDTOList = ordersMapper.selectTurnover(slectBegin, slectEnd, Orders.ORDER_STAUTS_COMPLETED);
        //进行数据的封装
        Map<String, BigDecimal> map = turnoverReportDTOList.stream().collect(Collectors.toMap(TurnoverReportDTO::getOrderDate, TurnoverReportDTO::getOrderMoney));
        List<BigDecimal> collect = dateList.stream().map(localDate -> {
            return map.get(localDate) == null ? new BigDecimal("0") : map.get(localDate);
        }).collect(Collectors.toList());
        return new TurnoverReportVO(dateList,collect);
    }

}
