package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishAndSetmealMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.utils.BeanHelper;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 9:24
 * @Description:
 */
@Service
@Slf4j
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private DishAndSetmealMapper dishAndSetmealMapper;

    /**
     * 根据分类id查询套餐列表
     * @param categoryId
     * @return
     */
    @Override
    @Cacheable(cacheNames = "user:setmeal",key = "#a0",unless ="#result==null")
    public List<Setmeal> selectByCategoryId(Long categoryId) {
       List<Setmeal> setmeals= setmealMapper.selectByCategoryId(categoryId);
        return setmeals;
    }

    /**
     * 套餐启停状态
     * @param status
     * @param id
     */
    @Override
    @CacheEvict(cacheNames = "user:setmeal",allEntries = true)
    public void updateStatus(Integer status, Long id) {
        //判断套餐的状态
        Long count = setmealMapper.selectStatus(Collections.singletonList(id));
        if (count > 0) {
            Setmeal build = Setmeal.builder().id(id).status(status).build();
            setmealMapper.updateStatus(build);
        } else {
            log.info("套餐为停售状态");
            //判断是否有关联菜品起售
            Long onDishCount = dishAndSetmealMapper.selectStatusBySetmeal(id);
            if (onDishCount > 0) {
                log.info("套餐有关联菜品停售无法起售");
                throw new BusinessException(MessageConstant.SETMEAL_ENABLE_FAILED);
            }
                Setmeal build = Setmeal.builder().id(id).status(status).build();
                setmealMapper.updateStatus(build);
            }
            //套餐可以进行起售
        }

    /**
     * 修改套餐
     * @param setmealDTO
     */
    @Override
    @Transactional
    @CacheEvict(cacheNames = "user:setmeal",allEntries = true)
    public void update(SetmealDTO setmealDTO) {
        log.info("修改套餐");
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
         setmealMapper.updateStatus(setmeal);
        //修改菜品关系表
        //删除
        dishAndSetmealMapper.delete(Collections.singletonList(setmeal.getId()));
        //添加
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (!CollectionUtils.isEmpty(setmealDishes)){
            setmealDishes.forEach(setmealDish -> setmealDish.setSetmealId(setmeal.getId()));
            dishAndSetmealMapper.add(setmealDishes);
        }
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    public SetmealVO selectById(Long id) {
        //根据id查询
     Setmeal setmeal= setmealMapper.selectById(id);
     //查询套餐菜品
      List<SetmealDish> SetmealDishes = dishAndSetmealMapper.selectBySetmeal(id);
        //拼接
        SetmealVO setmealVO = BeanHelper.copyProperties(setmeal, SetmealVO.class);
        setmealVO.setSetmealDishes(SetmealDishes);
        return setmealVO;
    }

    /**
     * 批量删除
     * @param ids
     */
    @Override
    @Transactional
    @CacheEvict(cacheNames = "user:setmeal",allEntries = true)
    public void deleteByIds(List<Long> ids) {
        //判断套餐是否起售
      Long count=  setmealMapper.selectStatus(ids);
      if (count>0){
          log.info("套餐在售无法删除");
          throw new BusinessException(MessageConstant.SETMEAL_ON_SALE);
      }
      //删除套餐
      setmealMapper.delete(ids);
      //删除套餐删除中间表
        dishAndSetmealMapper.delete(ids);
    }

    /**
     * 套餐分页
     * @param setmealPageQueryDTO
     * @return
     */
    @Override
    public PageResult selectPage(SetmealPageQueryDTO setmealPageQueryDTO) {
        SetmealVO setmeal = BeanHelper.copyProperties(setmealPageQueryDTO, SetmealVO.class);
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
       List<SetmealVO> setmeals= setmealMapper.selectPage(setmeal);
        Page<SetmealVO> setmealPage = (Page<SetmealVO>) setmeals;
        return new PageResult(setmealPage.getTotal(),setmealPage.getResult());
    }

    /**
     * 新增套餐
     * @param setmealDTO
     */
    @Override
    @Transactional
    @CacheEvict(cacheNames = "user:setmeal",key = "#a0.categoryId")
    public void addSetmeal(SetmealDTO setmealDTO) {
        //传入套餐基础信息
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmealMapper.addSetmeal(setmeal);
        //添加套餐菜品关系表
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        System.out.println(setmeal.getId());
        //判断是否为空
        if (CollectionUtils.isEmpty(setmealDishes)){
            log.info("套餐内没有添加菜品信息");
            return;
        }
        //遍历集合
        setmealDishes.forEach(setmealDish -> setmealDish.setSetmealId(setmeal.getId()));
        dishAndSetmealMapper.add(setmealDishes);
    }
}
