package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.*;
import com.sky.entity.AddressBook;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.entity.ShoppingCart;
import com.sky.exception.BusinessException;
import com.sky.mapper.OrderDetailsMapper;
import com.sky.mapper.OrdersMapper;
import com.sky.mapper.UserAddressBookMapper;
import com.sky.mapper.UserShopCartMapper;
import com.sky.result.Location;
import com.sky.result.PageResult;
import com.sky.service.OrdersService;
import com.sky.utils.BeanHelper;
import com.sky.utils.HttpClientUtil;
import com.sky.utils.MapUtils;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.vo.OrdersListVo;
import com.sky.web.WebSocketServer;
import javassist.expr.NewArray;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author：lizhijie
 * @Date：2023/6/29 12:26
 * @Description:
 */
@Service
@Slf4j
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    private UserAddressBookMapper addressBookMapper;
    @Autowired
    private UserShopCartMapper userShopCartMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderDetailsMapper orderDetailsMapper;
    @Autowired
    private WebSocketServer webSocketServer;
    @Autowired
    private MapUtils mapUtils;
    private final static String ADDRESS_URL ="https://api.map.baidu.com/geocoding/v3";

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @Override
    public OrdersListVo selectById(Long id) {
        Orders orders = Orders.builder().id(id).userId(BaseContext.getCurrentId()).build();
        OrdersListVo ordersListVo = ordersMapper.selectById(orders);
        return ordersListVo;
    }

    @Override
    public PageResult adminList(OrdersPageQueryDTO ordersPageQueryDTO) {
        //调用pagehelper查询订单信息
        PageHelper.startPage(ordersPageQueryDTO.getPage(),ordersPageQueryDTO.getPageSize());
        //紧接查询语句
        List<OrdersListVo> ordersListVoList=ordersMapper.selectByCondition(ordersPageQueryDTO);
        for (OrdersListVo ordersListVo : ordersListVoList) {
            List<OrderDetail> orderDetailList = orderDetailsMapper.selectByNumber(ordersListVo.getNumber());
            String dish="";
            for (OrderDetail detail : orderDetailList) {
                dish+=detail.getName()+"*"+detail.getNumber();
            }
            ordersListVo.setOrderDishes(dish);
        }
         Page<OrdersListVo> page= (Page<OrdersListVo>) ordersListVoList;

        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 完成订单
     * @param id
     */
    @Override
    public void completeOrder(Long id) {
//        - 判断订单是否存在 以及 订单的状态是否正确 （只有订单状态为 **派送中** 的订单， 才可以进行完成订单）
        Orders orders = ordersMapper.selectOrderById(OrdersConfirmDTO.builder().id(id).build());
        if (ObjectUtils.isEmpty(orders)){
            log.info("无此订单");
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }if (!orders.getStatus().equals(4)){
            log.info("订单状态错误");
            throw new BusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
//        - 更新订单的状态为 **已完成**
        orders.setStatus(5);
        ordersMapper.updateStatus(orders);
    }

    /**
     * 派送订单
     * @param id
     */
    @Override
    public void deliveryOrder(Long id) {
        log.info("派送订单");
/*- - - 判断订单是否存在 以及 订单的状态是否正确 （只有订单状态为 **已接单** 的订单， 才可以进行派送）
- 更新订单的状态为 **派送中***/
        Orders orders = ordersMapper.selectOrderById(OrdersConfirmDTO.builder().id(id).build());
        if (ObjectUtils.isEmpty(orders)){
            log.info("无此订单");
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        if (!orders.getStatus().equals(3)){
            log.info("订单状态错误");
            throw new BusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //开始更新订单
        orders.setStatus(4);
        ordersMapper.updateStatus(orders);
    }

    /**
     * 取消订单操作
     * @param ordersCancelDTO
     */
    @Override
    public void adminCancelOrder(OrdersCancelDTO ordersCancelDTO) {
        log.info("开始取消订单操作");
//- 校验订单是否存在
        Orders orders = ordersMapper.selectOrderById(OrdersConfirmDTO.builder().id(ordersCancelDTO.getId()).build());
        if (ObjectUtils.isEmpty(orders)){
            log.info("订单不存在");
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
//- 如果订单的支付状态为 **已支付** , 是需要退款的 (调用weChatUtil.refund方法退款)， 且将支付状态改为 **退款**
        if (orders.getPayStatus().equals(2)){
            //进行微信退款
            orders.setPayStatus(3);
        }
//- 更新订单的状态为 **已取消**
        orders.setStatus(6);
        orders.setCancelReason(ordersCancelDTO.getCancelReason());
        orders.setCancelTime(LocalDateTime.now());
        ordersMapper.updateStatus(orders);
    }

    /**
     * 拒绝订单
     * @param ordersRejectionDTO
     */
    @Override
    public void refuseOrder(OrdersRejectionDTO ordersRejectionDTO) {
        OrdersConfirmDTO ordersConfirmDTO = OrdersConfirmDTO.builder().id(ordersRejectionDTO.getId()).build();
        Orders orders = ordersMapper.selectOrderById(ordersConfirmDTO);
        //- 订单只有存在 且 状态为2(待接单) 才可以接单
        if (!orders.getStatus().equals(2)){
            log.info("订单状态异常");
            throw new BusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //- 如果该订单的支付状态为 **已支付** ,拒单时, 是需要退款的 (调用weChatUtil.refund方法退款)， 且将支付状态改为 **退款**
        if (orders.getPayStatus().equals(1)){
            log.info("订单已支付");
            //调用微信退款
            orders.setPayStatus(3);
        }
        //- 更新订单的状态为 **已取消**
        orders.setStatus(6);
        orders.setRejectionReason(ordersRejectionDTO.getRejectionReason());
        ordersMapper.updateStatus(orders);
    }

    /**
     *
     * 接单操作
     */
    @Override
    public void receivingOrders(OrdersConfirmDTO orders) {
        log.info("开始接单操作");
        //判断订单是否支付
        Orders orders1 =   ordersMapper.selectOrderById(orders);
        if (orders1.getPayStatus().equals(0)){
           log.info("订单未支付无法接单");
           throw new BusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        //未支付
        orders1.setStatus(3);
        ordersMapper.updateStatus(orders1);
    }

    /**
     * 后台查询订单详情
     * @param id
     * @return
     */
    @Override
    public OrdersListVo selectDetailsById(Long id) {
        log.info("根据id查询订单详情");
        Orders orders = Orders.builder().id(id).build();
        OrdersListVo ordersListVo = ordersMapper.selectById(orders);
        return ordersListVo;
    }

    /**
     * 订单数量
     * @return
     */
    @Override
    public OrderStatisticsVO count() {

        //代接单2待配送3/配送中4
        List<Map<String,Object>> list = ordersMapper.selectCount();
        List<String> statusGoup = list.stream().map(map -> {
           return map.get("statusGoup").toString();
        }).collect(Collectors.toList());
        List<String> count = list.stream().map(map -> {
            return map.get("数量").toString();
        }).collect(Collectors.toList());
        Map<String,Long> map =new HashMap<>();
        for (int i = 0; i < statusGoup.size(); i++) {
            map.put(statusGoup.get(i),Long.valueOf(count.get(i)));
        }
        return OrderStatisticsVO.builder().confirmed(map.get("confirmed")).toBeConfirmed(map.get("toBeConfirmed"))
                .deliveryInProgress(map.get("deliveryInProgress")).build() ;
    }

    /**
     * 再来一车
     * @param id
     */
    @Override
    public void Repurchase(Long id) {
        //根据id读取到orders内的数据
        Orders orders = Orders.builder().userId(BaseContext.getCurrentId()).id(id).build();
        OrdersListVo ordersListVo = ordersMapper.selectById(orders);
        if (Objects.isNull(ordersListVo)){
            //如果查询结果为null
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        List<OrderDetail> orderDetailList = ordersListVo.getOrderDetailList();
         if (CollectionUtils.isEmpty(orderDetailList)){
             log.info("菜品列表为空");
             throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
         }
        for (OrderDetail orderDetail : orderDetailList) {
            ShoppingCart shopCart = ShoppingCart.builder().dishId(orderDetail.getDishId()).setmealId(orderDetail.getSetmealId())
                    .amount(orderDetail.getAmount()).createTime(LocalDateTime.now()).dishFlavor(orderDetail.getDishFlavor())
                    .image(orderDetail.getImage()).userId(BaseContext.getCurrentId()).name(orderDetail.getName()).build();
            userShopCartMapper.add(shopCart);
        }


    }

    /**
     * 取消订单
     * @param id
     */
    @Override
    public void cancelOrder(Long id) {
         //检验是否有此订单且查看状态
        Orders orders = Orders.builder().id(id).userId(BaseContext.getCurrentId()).build();
      Long count =  ordersMapper.selectStatusById(orders);
      //如果订单为待接单且已支付状态
        //取消订单
        orders.setStatus(6);
        ordersMapper.updateStatus(orders);
    }

    /**
     * 订单列表查询
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult list(OrdersPageQueryDTO ordersPageQueryDTO) {
        PageHelper.startPage(ordersPageQueryDTO.getPage(),ordersPageQueryDTO.getPageSize());
       List<OrdersListVo> ordersListVoList= ordersMapper.list(ordersPageQueryDTO.getStatus(),BaseContext.getCurrentId());
        Page page = (Page) ordersListVoList;
        return  new PageResult(page.getTotal(),page.getResult());
    }
    @Value("${sky.shop.address}")
    private String ShopAddress;
    /**
     * 添加订单
     * @param submitDTO
     * @return
     */
    @Override
    public OrderSubmitVO submit(OrdersSubmitDTO submitDTO) throws IOException {
        //判断地址是否含有数据
        AddressBook addressBook = AddressBook.builder().id(submitDTO.getAddressBookId()).build();
        addressBook = addressBookMapper.selectById(addressBook);
        if (Objects.isNull(addressBook)){
            //如果地址为空抛出异常
            log.info("地址为空");
            throw new BusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }
        //判断购物车数据是否为空
        List<ShoppingCart> shoppingCarts = userShopCartMapper.selectByUserId(ShoppingCart.builder().userId(BaseContext.getCurrentId()).build());
        if (CollectionUtils.isEmpty(shoppingCarts)){
            //如果购物车为空
            log.info("购物车数据为空");
            throw new BusinessException(MessageConstant.SHOPPING_CART_IS_NULL);
        }
        //开始判断地址的正确
      /* if (!checkAddress(addressBook)){
           log.info("超出范围");
           throw new BusinessException(MessageConstant.DELIVERY_OUT_OF_RANGE);
       }*/
        //拼接详细地址
        checkAddress(addressBook);
        //开始对顶单表内插入数据
        Orders orders = BeanHelper.copyProperties(submitDTO, Orders.class);
        orders.setNumber(String.valueOf(System.nanoTime()));
        orders.setUserId(BaseContext.getCurrentId());
        orders.setAddress(addressBook.getDetail());
        orders.setStatus(Orders.ORDER_STAUTS_TO_BE_CONFIRMED);
        orders.setOrderTime(LocalDateTime.now());
        orders.setPayStatus(Orders.PAY_STATUS_PAID);
        orders.setPhone(addressBook.getPhone());
        orders.setConsignee(addressBook.getConsignee());
        ordersMapper.add(orders);


       //开始对细节表内插入数据
        List<OrderDetail> orderDetails = shoppingCarts.stream().map(cart -> OrderDetail.builder()
                .orderId(Long.valueOf(orders.getNumber())).amount(cart.getAmount())
                .dishId(cart.getDishId()).setmealId(cart.getSetmealId())
                .name(cart.getName()).image(cart.getImage()).dishFlavor(cart.getDishFlavor())
                .number(cart.getNumber()).build()).collect(Collectors.toList());
        orderDetailsMapper.add(orderDetails);

         //清空购物车
        userShopCartMapper.deleteAllById(BaseContext.getCurrentId());
        //组装返回数据
        OrderSubmitVO orderSubmitVO = OrderSubmitVO.builder().orderAmount(orders.getAmount()).orderNumber(orders.getNumber())
                .orderTime(orders.getOrderTime()).id(orders.getId()).build();
        //开始对管理端客户发起提醒
        Map<String,Object> map = new HashMap<>();
        map.put("type",1);
        map.put("orderId",orders.getId());
        map.put("content",orders.getNumber());
        webSocketServer.sendMessage(JSON.toJSONString(map));
        return orderSubmitVO;
    }

    private void checkAddress(AddressBook addressBook) {
        String address = addressBook.getProvinceName() + addressBook.getDistrictName() + addressBook.getDetail();
        Location shopLocation = mapUtils.countAddress(ShopAddress);
        Location destinLocation = mapUtils.countAddress(address);
        Integer distance = mapUtils.countDistance(shopLocation.latAndlng(), destinLocation.latAndlng());
        if (distance>5000){
            log.info("距离点餐地点{},太远无法配送",distance);
            throw new BusinessException(MessageConstant.DELIVERY_OUT_OF_RANGE);
        }
    }




/*    private boolean checkAddress(AddressBook addressBook) {
        //判断地址是否在运营范围内
        String detail = addressBook.getDetail();
        //判断地址非空
        if (!StringUtils.hasLength(detail)){
            log.info("地址为空无法下单");
            new BusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }
        //检测地址
        Map<String,String> map = new HashMap<>();
        map.put("address",address);
        map.put("output","json");
        map.put("ak",ak);
        String address1 = HttpClientUtil.doGet(ADDRESS_URL, map);
        JSONObject address2  = JSON.parseObject(address1);
        if(address2.getInteger("status")!=0){
            log.info("地图解析异常");
            throw new BusinessException("地图解析异常");
        }
        JSONObject jsonObjectLocation = address2.getJSONObject("result").getJSONObject("location");
        String lng = jsonObjectLocation.get("lng").toString();
        String lat = jsonObjectLocation.get("lat").toString();
        StringBuilder stringBuilder = new StringBuilder();
        String shopLongitudeAndLatitude = stringBuilder.append(lat).append(",").append(lng).toString();
        Map<String,String> map2 = new HashMap<>();
        map2.put("address",detail);
        map2.put("output","json");
        map2.put("ak",ak);
        String address3 = HttpClientUtil.doGet(ADDRESS_URL, map2);
        JSONObject address4  = JSON.parseObject(address3);
        JSONObject jsonObjectLocation2 = address4.getJSONObject("result").getJSONObject("location");
        String lng1 = jsonObjectLocation2.get("lng").toString();
        String lat1 = jsonObjectLocation2.get("lat").toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        String addressLongitudeAndLatitude = stringBuilder2.append(lat1).append(",").append(lng1).toString();
        HashMap<String, String> params = new HashMap<>();
        params.put("origin", addressLongitudeAndLatitude);
        params.put("destination", shopLongitudeAndLatitude);
        params.put("ak", ak);
        String distance = HttpClientUtil.doGet("https://api.map.baidu.com/directionlite/v1/driving", params);
        //解析
        JSONObject resultObject = JSON.parseObject(distance);
        JSONArray routesObject  = resultObject.getJSONObject("result").getJSONArray("routes");
        JSONObject jsonObject = routesObject.getJSONObject(0);
        Long resultDistance = Long.valueOf(jsonObject.get("distance").toString());
        log.info("距离为{}",distance);
        if(resultDistance >= 5000L){
            return false;
        }
        return true;
    }*/
}
