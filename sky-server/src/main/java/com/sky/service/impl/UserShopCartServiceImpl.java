package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.UserShopCartMapper;
import com.sky.service.SetmealService;
import com.sky.service.UserShopCartService;
import com.sky.utils.BeanHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 20:52
 * @Description:
 */
@Service
@Slf4j
public class UserShopCartServiceImpl implements UserShopCartService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private UserShopCartMapper userShopCartMapper;

    /**
     * 减少商品数量
     * @param shoppingCartDTO
     */
    @Override
    public void subNumber(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        //对于目标用户购物车进行查询
      shoppingCart =  userShopCartMapper.selectNumberByUserId(shoppingCart);
        Integer number = shoppingCart.getNumber();
        if (number>1){
          //将商品数量减一
          shoppingCart.setNumber(number-1);
          //将数据库的数量更新
            userShopCartMapper.update(shoppingCart);
      }else {
            //将商品从购物车删除
            userShopCartMapper.deleteById(shoppingCart);
        }
    }

    /**
     * 清空购物车
     */
    @Override
    public void clean() {
        Long currentId = BaseContext.getCurrentId();
        userShopCartMapper.deleteAllById(currentId);
    }

    /**
     * 查询购物车
     * @return
     */
    @Override
    public List<ShoppingCart> list() {
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        return  userShopCartMapper.selectByUserId(shoppingCart);
    }

    /**
     * 添加购物车
     * @param shoppingCartDTO
     */
    @Override
    public void addShopCard(ShoppingCartDTO shoppingCartDTO) {
        //首先拿到用户id
        Long currentId = BaseContext.getCurrentId();
        //通过用户id查询购物车
        ShoppingCart shoppingCart1 = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart1.setUserId(currentId);
        List<ShoppingCart> shoppingCarts = userShopCartMapper.selectByUserId(shoppingCart1);
        if (!CollectionUtils.isEmpty(shoppingCarts)) {
            //购物车已经有这个商品直接数量加一
            ShoppingCart cart = shoppingCarts.get(0);
            cart.setNumber(cart.getNumber() + 1);
            userShopCartMapper.update(cart);
        } else {
            //购物车内没有此数据直接添加
            //判断购物车内是菜品还是套餐
            if (shoppingCartDTO.getDishId() == null) {
                //说明他是套餐
                Setmeal setmeal = Setmeal.builder().id(shoppingCartDTO.getSetmealId()).build();
                setmeal = setmealMapper.selectById(setmeal.getId());
                ShoppingCart shoppingCart = ShoppingCart.builder().amount(setmeal.getPrice()).createTime(LocalDateTime.now()).
                        name(setmeal.getName()).userId(BaseContext.getCurrentId()).image(setmeal.getImage()).setmealId(setmeal.getId()).build();
                userShopCartMapper.add(shoppingCart);
            } else {
                //是菜品
                log.info("是菜品");
                Dish dish = Dish.builder().id(shoppingCartDTO.getDishId()).build();
                dish = dishMapper.selectById(dish.getId());
                ShoppingCart shoppingCart = ShoppingCart.builder().amount(dish.getPrice()).createTime(LocalDateTime.now()).dishFlavor(shoppingCartDTO.getDishFlavor()
                ).name(dish.getName()).userId(BaseContext.getCurrentId()).image(dish.getImage()).dishId(dish.getId()).build();
                userShopCartMapper.add(shoppingCart);
            }

        }
    }
}
