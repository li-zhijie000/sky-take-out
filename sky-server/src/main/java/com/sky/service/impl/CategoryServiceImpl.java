package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.Employee;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import com.sky.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @Author：lizhijie
 * @Date：2023/6/21 18:06
 * @Description:
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 根据类型查询分类列表
     * @param type
     * @return
     */
    @Override
    public List<Category> listByType(Integer type) {
        //先查询缓存数据库
       List<Category>categoryList= categoryMapper.listByType(type);
        return categoryList;
    }

    @Override
    public List<Category> selectByType(Integer type) {
        List<Category>  categoryList=  categoryMapper.selectByType(type);
        return categoryList;
    }

    /**
     * 删除分类如果下有菜品或套餐无法删除
     * @param id
     */
    @Override
    public void delete(Long id) {
        String name="";
     List<Dish> dishes= dishMapper.selectByCategoryId(id,name);
     if (!dishes.isEmpty()){
         throw new BusinessException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
     }
        List<Setmeal> setmeals= setmealMapper.selectByCategoryId(id);
     if (!setmeals.isEmpty()){
         throw new BusinessException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
     }
     categoryMapper.delete(id);
    }

    @Override
    public void add(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
        category.setStatus(0);
//        category.setUpdateTime(LocalDateTime.now());
//        category.setCreateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
//        category.setCreateUser(BaseContext.getCurrentId());
        categoryMapper.add(category);
    }

    @Override
    public PageResult list(CategoryPageQueryDTO categoryPageQueryDTO) {
        PageHelper.startPage(categoryPageQueryDTO.getPage(),categoryPageQueryDTO.getPageSize());
     List<Category> categoryList= categoryMapper.list(categoryPageQueryDTO.getName(),categoryPageQueryDTO.getType());
        Page page = (Page) categoryList;
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public Category selectById(Long id) {
     Category category=   categoryMapper.selectById(id);
        return category;
    }

    @Override
    public void update(CategoryDTO categoryDTO) {
        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        category.setStatus(StatusConstant.DISABLE);
        categoryMapper.update(category);
    }

    @Override
    public void categoryStatus(Integer status, Long id) {
        Category category = Category.builder().id(id).status(status).
//                updateTime(LocalDateTime.now()).
//                updateUser(BaseContext.getCurrentId()).
                build();
        categoryMapper.update(category);
    }
}
