package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.*;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishAndSetmealMapper;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 16:06
 * @Description:
 */
@Service
@Slf4j
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
   private DishAndSetmealMapper dishAndSetmealMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 修改菜品属性
     * @param dishDTO
     */
    @Override
    public void update(DishDTO dishDTO) {
        //属性转移
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        //对于菜品进行修改
       dishMapper.update(dish);
       //对菜品口味信息进行删除
        dishFlavorMapper.delete(Collections.singletonList(dishDTO.getId().intValue()));
        //对于菜品口味信息添加
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (!CollectionUtils.isEmpty(flavors)){
            //集合不为空
            flavors.forEach(flavor->flavor.setDishId(dish.getId()));
            dishFlavorMapper.add(flavors);
        }

    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public List<DishItemVO> selectBySetmealId(Long id) {
        //查询菜品列表
      List<DishItemVO> DishItemVOes=  dishMapper.selectBySetmealId(id);
        return DishItemVOes;
    }

    /**
     * 查找分类id对应菜品
     * @param id
     * @return
     */
    @Override
    public List<DishVO> selectByCategoryId(Long id) {
        //优先查询缓存中数据
        String key = "dish:cache:" + id;
        List<DishVO> dishVOList = (List<DishVO>) redisTemplate.opsForValue().get(key);
        if (!CollectionUtils.isEmpty(dishVOList)){
            log.info("命中缓存,直接从缓存中读取数据");
            return dishVOList;
        }
        String name = "";
        List<Dish> dishes = dishMapper.selectByCategoryId(id,name);
        if (CollectionUtils.isEmpty(dishes)){
            return null;
        }
        dishVOList = new ArrayList<>();
        //查询口味表
        for (Dish dish : dishes) {
            List<DishFlavor> dishFlavorList = dishFlavorMapper.selectByDishId(dish.getId());
                    DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);
                    dishVO.setFlavors(dishFlavorList);
                    dishVOList.add(dishVO);
        }
        //将数据库查询数据缓存进入Redis
        log.info("将数据库中数据添加入缓存中");
        redisTemplate.opsForValue().set(key,dishVOList);
        return dishVOList;
    }

    /**
     * 菜品列表查询
     * @param categoryId
     * @param name
     * @return
     */
    @Override
    public List<Dish> list(Integer categoryId, String name) {
        //通过菜品分类查询菜品
        Long id = categoryId.longValue();
      List<Dish> dishes=  dishMapper.selectByCategoryId(id,name);
        return dishes;
    }

    /**
     * 菜品状态转换
     * @param status
     * @param id
     */
    @Transactional
    @Override
    public void dishStatus(Integer status, Long id) {
      //如果停售菜品,菜品相关套餐也需要停售
        List<Long> longs = dishAndSetmealMapper.selectSetmealByDishid(Collections.singletonList(id.intValue()));
        List<Setmeal> setmeals = new ArrayList<>();
        for (Long aLong : longs) {
            Setmeal setmeal = Setmeal.builder().status(StatusConstant.DISABLE).id(aLong).build();
            setmeals.add(setmeal);
        }
        for (Setmeal setmeal : setmeals) {
            //停售相关套餐
            setmealMapper.updateStatus(setmeal);
        }
        //停售菜品
        Dish build = Dish.builder().id(id).status(status).build();
        dishMapper.update(build);
        //更新后需要删除缓存数据
        cleanCache("*");
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    public DishVO selectById(Long id) {
        //查询菜品列表
      Dish dish=  dishMapper.selectById(id);
      //查询菜品口味列表
      List<DishFlavor> dishFlavorList= dishFlavorMapper.selectByDishId(id);
        DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);
        dishVO.setFlavors(dishFlavorList);
        return dishVO;
    }

    /**
     * 批量删除
     * @param ids
     */
    @Override
    @Transactional
    public void delete(List<Integer> ids) {
        //判断菜品是否起售
        Long count=dishMapper.selectCountById(ids);
        if (count>0){
            log.info("菜品为起售状态");
            throw new BusinessException(MessageConstant.DISH_ON_SALE);
        }
        //判断菜品是否绑定了套餐
     List<Long> list= dishAndSetmealMapper.selectSetmealByDishid(ids);
        if (!CollectionUtils.isEmpty(list)){
            log.info("菜品绑定了套餐");
            throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        //菜品和菜单都判断完成删除
        dishMapper.delete(ids);
        dishFlavorMapper.delete(ids);
        //更新后需要删除缓存数据
        cleanCache("*");
    }



    /**
     *
     * 分页查询
     * @param dto
     * @return
     */
    @Override
    public PageResult pageList(DishPageQueryDTO dto) {
        //进行分页
        PageHelper.startPage(dto.getPage(),dto.getPageSize());
        //进行查询数据
        DishVO dishVO = BeanHelper.copyProperties(dto, DishVO.class);
      List<DishVO> dishVOList=  dishMapper.pageList(dishVO);
        Page<DishVO> page = (Page<DishVO>) dishVOList;
        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 新增菜品
     * @param dishDTO
     */
    @Override
    @Transactional
    public void addDish(DishDTO dishDTO) {
        //新增菜品
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        dishMapper.addDish(dish);
        //新增口味
        List<DishFlavor> flavors = dishDTO.getFlavors();
        flavors.forEach(flavor->flavor.setDishId(dish.getId()));
        dishFlavorMapper.add(flavors);
        //需要删除当前缓存
        cleanCache(dishDTO.getCategoryId().toString());
    }
    private void cleanCache(String suffix) {
        log.info("删除全部缓存");
        Set keys = redisTemplate.keys("dish:cache:"+suffix);
        redisTemplate.delete(keys);
    }
}
