package com.sky.service.impl;

import com.sky.constant.MessageConstant;
import com.sky.exception.BusinessException;
import com.sky.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 15:12
 * @Description:
 */
@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void setStatus(Integer status) {
        redisTemplate.opsForValue().set("status",status);
    }

    @Override
    public Integer getStatus() {
        Object status = redisTemplate.opsForValue().get("status");
        if (Objects.isNull(status)){
            throw new BusinessException(MessageConstant.STATUS_ACCOUNT);
        }
        return Integer.valueOf(status.toString());
    }
}
