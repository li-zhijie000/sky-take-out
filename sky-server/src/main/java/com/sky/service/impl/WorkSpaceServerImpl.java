package com.sky.service.impl;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.*;
import com.sky.service.WorkSpaceServer;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 19:41
 * @Description:
 */
@Service
@Slf4j
public class WorkSpaceServerImpl implements WorkSpaceServer {
    @Autowired
    private OrdersMapper ordersMapper;
   @Autowired
   private DishMapper dishMapper;
   @Autowired
   private SetmealMapper setmealMapper;

    @Override
    public SetmealOverViewVO selectSetmealAll() {
        SetmealOverViewVO setmealOverViewVO=  setmealMapper.selectSetmealAll();
        return setmealOverViewVO;
    }

    /**
     * 查询菜品总览
     * @return
     */
    @Override
    public DishOverViewVO selectDishAll() {
        //查询菜品信息
        DishOverViewVO dish=  dishMapper.selectByStatus();
        return dish;
    }

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询今日订单信息
     * @return
     */
    @Override
    public OrderOverViewVO todayOrderInfo() {
        log.info("查询今日订单信息");
       //今日日期
        LocalDate now = LocalDate.now();
        LocalDateTime min = LocalDateTime.of(now, LocalTime.MIN);
        LocalDateTime max = LocalDateTime.of(now, LocalTime.MAX);
        List<Map<String,Object>> maps = ordersMapper.selectTodayOrder(min,max);
        List<String> longList = maps.stream().map(map -> {
            return map.get("状态").toString();
        }).collect(Collectors.toList());
        List<String> countList = maps.stream().map(map -> {
            return map.get("数量").toString();
        }).collect(Collectors.toList());
        Map<String,String> map  =new HashMap<>();
        for (int i = 0; i < longList.size(); i++) {
            map.put(longList.get(i),countList.get(i));
        }
        Collection<String> values = map.values();
        Integer allOrders =0;
        for (String value : values) {
            allOrders+=Integer.parseInt(value);
        }
        return OrderOverViewVO.builder().allOrders(allOrders).cancelledOrders(Integer.parseInt( map.get("cancelledOrders")==null?"0":map.get("cancelledOrders")))
                .completedOrders(Integer.valueOf(map.get("completedOrders")==null?"0":map.get("completedOrders"))).deliveredOrders(Integer.valueOf(map.get("deliveredOrders")==null?"0":map.get("deliveredOrders")))
                .waitingOrders(Integer.parseInt(map.get("waitingOrders")==null?"0":map.get("waitingOrders"))).build();
    }

    @Override
    public BusinessDataVO selectInfo() {
        log.info("开始查询运营数据");
        LocalDate today = LocalDate.now();
        LocalDateTime min = LocalDateTime.of(today, LocalTime.MIN);
        LocalDateTime max = LocalDateTime.of(today, LocalTime.MAX);
        //进行数据的接收
        BusinessDataVO businessDataVO = ordersMapper.selectDataByDate(min, max);
        //对于今日运营数据进行封装
       Integer newUsers =CollectionUtils.isEmpty(userMapper.selectAddCount(min, max))? 0:userMapper.selectAddCount(min, max).get(0).getUserCount();
       businessDataVO.setNewUsers(newUsers);
         return businessDataVO;
    }
}
