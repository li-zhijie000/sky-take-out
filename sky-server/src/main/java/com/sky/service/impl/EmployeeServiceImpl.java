package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.exception.DataException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.BeanHelper;
import com.sky.vo.EmployeeLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {
@Autowired
private EmployeeMapper employeeMapper;
@Autowired
private RedisTemplate redisTemplate;

private static final String LOCK_ADMIN_ERROR="lock:admin:error";
private static final String LOGIN_ADMIN="login:admin:";
    @Override
    public Employee employeeLogin(EmployeeLoginDTO emp) {
        Employee employee=employeeMapper.employeeLogin(emp.getUsername());
        //校验是否为空
        if (Objects.isNull(employee)){
            log.info("用户名不存在");
            throw  new DataException(MessageConstant.ACCOUNT_LOCKED);
        }
        String rightPassword = employee.getPassword();
        String username = employee.getUsername();
        //首先校验是否被封禁
        Object o = redisTemplate.opsForValue().get(LOCK_ADMIN_ERROR + username);
        if (Objects.nonNull(o)) {
            log.info("账户{}被锁定",username);
            throw new BusinessException(MessageConstant.LOCKED_ACCOUNT_ERROR);
        }
        //判断密码是否正确
      String  userPassword = DigestUtils.md5Hex(emp.getPassword().getBytes(StandardCharsets.UTF_8));

        if (!rightPassword.equals(userPassword)){
            log.info("密码错误");
            //如果密码错误需要加一个key
            redisTemplate.opsForValue().set(LOGIN_ADMIN+username+ RandomStringUtils.random(5),"-",5, TimeUnit.MINUTES);
            Set keys = redisTemplate.keys(LOGIN_ADMIN+"*");
            if (keys.size()>=5){
                redisTemplate.opsForValue().set(LOCK_ADMIN_ERROR+username,"-",1,TimeUnit.HOURS);
                log.info("账户{}被锁定",username);
                throw new BusinessException(MessageConstant.LOCKED_ACCOUNT_ERROR);
            }
            throw new BusinessException(MessageConstant.PASSWORD_ERROR);
        }
        //校验员工是否启用
        if (employee.getStatus().equals(StatusConstant.DISABLE)){
            log.info("员工没有启用");
            throw new BusinessException(MessageConstant.ACCOUNT_LOCKED);
        }

        return employee;
    }

    @Override
    public Employee employeeById(Long id) {
      Employee employee=  employeeMapper.employeeById(id);
        return employee;
    }

    @Override
    public void update(EmployeeDTO employeeDTO) {
        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);
        employeeMapper.update(employee);
    }

    /**
     * 修改员工状态
     * @param status
     * @param id
     */
    @Override
    public void employeeStatus(Integer status, Long id) {
        Employee employee = Employee.builder().id(id).status(status).
//                updateTime(LocalDateTime.now())
//                .updateUser(BaseContext.getCurrentId()).
                        build();
        employeeMapper.update(employee);
    }

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */
    @Override
    public PageResult page(EmployeePageQueryDTO employeePageQueryDTO) {
        PageHelper.startPage(employeePageQueryDTO.getPage(),employeePageQueryDTO.getPageSize());
        List<Employee> employeeList = employeeMapper.list(employeePageQueryDTO.getName());
        Page<Employee> page = (Page<Employee>) employeeList;
        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 新增员工
     * @param employeeDTO
     */
    @Override
   public void addEmployee(EmployeeDTO employeeDTO) {
        //数据传输对象
        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);
        //给新对象赋值
//        Long currentId = BaseContext.getCurrentId();
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setCreateUser(currentId);
//        employee.setUpdateUser(currentId);
        employee.setPassword(DigestUtils.md5Hex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        employee.setStatus(StatusConstant.ENABLE);
        employeeMapper.addEmployee(employee);
    }
}
