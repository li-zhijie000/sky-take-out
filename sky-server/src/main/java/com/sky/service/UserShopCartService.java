package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 20:47
 * @Description:
 */

public interface UserShopCartService {

    void addShopCard(ShoppingCartDTO shoppingCartDTO);

    List<ShoppingCart> list();

    void clean();

    void subNumber(ShoppingCartDTO shoppingCartDTO);
}
