package com.sky.service;

import com.sky.dto.*;
import com.sky.entity.Orders;
import com.sky.result.PageResult;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.vo.OrdersListVo;

import java.io.IOException;
import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/29 12:26
 * @Description:
 */
public interface OrdersService {
    OrderSubmitVO submit(OrdersSubmitDTO submitDTO) throws IOException;

    PageResult list(OrdersPageQueryDTO ordersPageQueryDTO);

    OrdersListVo selectById(Long id);

    void cancelOrder(Long id);

    void Repurchase(Long id);

    PageResult adminList(OrdersPageQueryDTO ordersPageQueryDTO);

    OrderStatisticsVO count();

    OrdersListVo selectDetailsById(Long id);

    void receivingOrders(OrdersConfirmDTO orders);

    void refuseOrder(OrdersRejectionDTO ordersRejectionDTO);

    void adminCancelOrder(OrdersCancelDTO ordersCancelDTO);

    void deliveryOrder(Long id);

    void completeOrder(Long id);
}
