package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.SetmealVO;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 9:24
 * @Description:
 */
public interface SetmealService {
    void addSetmeal(SetmealDTO setmealDTO);

    PageResult selectPage(SetmealPageQueryDTO setmealPageQueryDTO);

    void deleteByIds(List<Long> ids);

    SetmealVO selectById(Long id);

    void update(SetmealDTO setmealDTO);

    void updateStatus(Integer status, Long id);

    List<Setmeal> selectByCategoryId(Long categoryId);
}
