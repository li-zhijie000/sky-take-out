package com.sky.service;

import com.sky.entity.AddressBook;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/27 17:43
 * @Description:
 */
public interface UserAddressBookService {
    void add(AddressBook addressBook);

    List<AddressBook> list();

    AddressBook selectById(Long id);

    void update(AddressBook addressBook);

    void delete(Long id);

    void defaultAddress(AddressBook dto);

    AddressBook selectDefaultAddress();
}
