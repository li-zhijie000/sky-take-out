package com.sky.service;


import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;
import com.sky.vo.EmployeeLoginVO;

public interface EmployeeService {

    Employee employeeLogin(EmployeeLoginDTO emp);

    void addEmployee(EmployeeDTO employeeDTO);

    PageResult page(EmployeePageQueryDTO employeePageQueryDTO);

    void employeeStatus(Integer status, Long id);

    Employee employeeById(Long id);

    void update(EmployeeDTO employeeDTO);
}
