package com.sky.service;

import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 16:15
 * @Description:
 */
public interface UserService {
    User userLogin(UserLoginDTO userLoginDTO);
}
