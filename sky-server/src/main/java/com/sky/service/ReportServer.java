package com.sky.service;

import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;

import java.io.IOException;
import java.time.LocalDate;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 11:22
 * @Description:
 */
public interface ReportServer {
    TurnoverReportVO selectTurnover(LocalDate begin, LocalDate end);

    UserReportVO userCount(LocalDate begin, LocalDate end);

    OrderReportVO ordersCount(LocalDate begin, LocalDate end);

    SalesTop10ReportVO salesTop(LocalDate begin, LocalDate end);

    void export() throws IOException;
}
