package com.sky.service;

import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 19:41
 * @Description:
 */
public interface WorkSpaceServer {
    BusinessDataVO selectInfo();

    OrderOverViewVO todayOrderInfo();

    DishOverViewVO selectDishAll();

    SetmealOverViewVO selectSetmealAll();
}
