package com.sky.service;

import com.github.pagehelper.PageHelper;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/21 18:06
 * @Description:
 */
public interface CategoryService {
    PageResult list(CategoryPageQueryDTO categoryPageQueryDTO);

    Category selectById(Long id);

    void update(CategoryDTO categoryDTO);

    void categoryStatus(Integer status, Long id);

    void add(CategoryDTO categoryDTO);

    void delete(Long id);

    List<Category> selectByType(Integer type);

    List<Category> listByType(Integer type);
}
