package com.sky.task;

import com.sky.constant.MessageConstant;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/7/2 12:02
 * @Description:
 */
@Component
@Slf4j
public class OrderTask {
    @Autowired
    private OrdersMapper ordersMapper;
    //订单超时自动取消
    @Scheduled(cron = "0/30 * * * * ?")
    public void  overOrders(){
        //首先对于当前时间进行判断
        LocalDateTime localDateTime = LocalDateTime.now().minusMinutes(15);
        //进行查询
      List<Orders> ordersList= ordersMapper.selectOverOrders(Orders.PAY_STATUS_PAID,localDateTime);//传入1代表状态为未支付
        //对于集合进行非空判断是否有条件
        if (CollectionUtils.isEmpty(ordersList)) {
            log.info("没有超时的订单");
            return;
        }
        //非空进行遍历修改订单状态
        ordersList.stream().forEach(orders -> {
            orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);
            orders.setCancelReason(MessageConstant.ORDER_OUT_TIME);
            orders.setCancelTime(LocalDateTime.now());
            ordersMapper.updateStatus(orders);
        });
    }
    //在晚上1点定时将待配送的订单转变为完成状态
    @Scheduled(cron = "0 0 1 * * ?")
    public void inishOrders(){
        //查询是否有满足条件的订单要求
        LocalDateTime before2hTime = LocalDateTime.now().minusHours(2);

        List<Orders> ordersList = ordersMapper.selectOverOrders(Orders.ORDER_STAUTS_TO_BE_CONFIRMED, before2hTime);
        if (CollectionUtils.isEmpty(ordersList)){
            log.info("没有满足要求的订单");
            return;
        }
        //有满足要求的订单开始遍历集合进行修改状态
        for (Orders orders : ordersList) {
            log.info("开始对于id为{}订单修改状态",orders.getId());
            orders.setStatus(Orders.ORDER_STAUTS_COMPLETED);
            orders.setDeliveryTime(LocalDateTime.now());
            //调用mapper进行状态的修改
            ordersMapper.updateStatus(orders);
        }

    }
}
