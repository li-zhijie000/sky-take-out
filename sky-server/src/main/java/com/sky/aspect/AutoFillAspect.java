package com.sky.aspect;

import com.sky.annotation.AutoFillAnnotation;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.binding.MapperMethod;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 10:59
 * @Description:
 */
@Aspect
@Component
@Slf4j
public class AutoFillAspect {
    @Before("@annotation(com.sky.annotation.AutoFillAnnotation)&&execution(* com.sky.mapper.*.*(..) )")
    public void autoFill(JoinPoint joinPoint) throws Exception {
          //拿到传入参数
        Object[] args = joinPoint.getArgs();
        //判断传入参数是否为空
        if (ObjectUtils.isEmpty(args)){
            log.info("传入参数为空");
            return;
        }
        //拿取到对象
        Object obj = args[0];
        //通过反射拿到方法
        Method setCreateTime = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
        Method setUpdateTime = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
        Method setCreateUser = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class);
        Method setUpdateUser = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);
         //判断方法
       MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        AutoFillAnnotation annotation = signature.getMethod().getAnnotation(AutoFillAnnotation.class);
        //开始注入
        if (annotation.value().equals(OperationType.INSERT)){
            setCreateTime.invoke(obj,LocalDateTime.now());
            setCreateUser.invoke(obj, BaseContext.getCurrentId());
        }
        setUpdateTime.invoke(obj,LocalDateTime.now());
        setUpdateUser.invoke(obj,BaseContext.getCurrentId());
    }
}
