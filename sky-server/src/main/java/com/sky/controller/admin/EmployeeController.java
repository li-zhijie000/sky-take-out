package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理Controller
 */
@Slf4j
@RestController
@Api(tags = "登录的类")
@RequestMapping("/admin/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;
    //登录接口
    @PostMapping("/login")
    @ApiOperation("登录接口")
    public Result<EmployeeLoginVO> employeeLogin(@RequestBody EmployeeLoginDTO emp){
        log.info("员工登录{}",emp);
       Employee employee = employeeService.employeeLogin(emp);
       //对于账号开始发放jwt令牌
        Map<String,Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID,employee.getId());
        String jwt = JwtUtil.createJWT(jwtProperties.getAdminSecretKey(), jwtProperties.getAdminTtl(), claims);
        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder().id(employee.getId())
                .name(employee.getName())
                .userName(employee.getUsername())
                .token(jwt).build();

        return Result.success(employeeLoginVO);

    }
    //新增员工
    @PostMapping
    @ApiOperation("新增员工接口")
    public Result addEmployee(@RequestBody EmployeeDTO employeeDTO){
        log.info("新增员工");
        employeeService.addEmployee(employeeDTO);
        return Result.success();
    }
 //分页查询员工

    @GetMapping("/page")
    public Result<PageResult> page(EmployeePageQueryDTO employeePageQueryDTO){
      PageResult pageResult= employeeService.page(employeePageQueryDTO);
        return Result.success(pageResult);
    }
    //启用员工
    @PutMapping("/status/{status}/{id}")
    public Result employeeStatus(@PathVariable Integer status,@PathVariable Long id){
        employeeService.employeeStatus(status,id);
        return Result.success();
    }
    //根据id查询
    @GetMapping("/{id}")
    public Result employeeById(@PathVariable Long id){
       Employee employee= employeeService.employeeById(id);
       return Result.success(employee);
    }
    //修改员工
    @PutMapping
    public Result update(@RequestBody EmployeeDTO employeeDTO){
        log.info("修改员工");
        employeeService.update(employeeDTO);
        return Result.success();
    }
}