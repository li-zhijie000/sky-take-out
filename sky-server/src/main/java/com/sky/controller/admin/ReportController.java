package com.sky.controller.admin;

import com.sky.mapper.ReportMapper;
import com.sky.result.Result;
import com.sky.service.ReportServer;
import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 11:17
 * @Description:
 */
@RestController
@Slf4j
@RequestMapping("/admin/report")
public class ReportController {
    @Autowired
    private ReportServer reportServer;
    //营业额统计
    @GetMapping("/turnoverStatistics")
    public Result turnoverCount(@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate begin,@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate end){
        log.info("开始日期{}到{}的营业额统计",begin,end);
      TurnoverReportVO turnoverReportVO= reportServer.selectTurnover(begin,end);
        return Result.success(turnoverReportVO);
    }
    //用户统计
    @GetMapping("/userStatistics")
    public Result userCount(@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate begin,@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate end){
        log.info("用户统计");
        UserReportVO userReportVO =  reportServer.userCount(begin,end);
        return  Result.success(userReportVO);
    }
    //订单统计
    @GetMapping("/ordersStatistics")
    public Result ordersCount(@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate begin,@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate end){
        log.info("开始进行订单数量的统计");
      OrderReportVO orderReportVO= reportServer.ordersCount(begin,end);
      return Result.success(orderReportVO);
    }
    //统计菜品销量前10
    @GetMapping("/top10")
    public Result salesTop(@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate begin,@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate end){
        log.info("开始统计菜品销量前10");
        SalesTop10ReportVO salesTop10ReportVO=   reportServer.salesTop(begin,end);
        return Result.success(salesTop10ReportVO);
    }
    //导出Excel
    @GetMapping("/export")
    public void  export() throws Exception {
      log.info("开始导出Excel表格");
      reportServer.export();
    }
}
