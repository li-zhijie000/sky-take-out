package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 9:19
 * @Description:
 */
@RestController
@RequestMapping("/admin/setmeal")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService setmealService;
    @PostMapping
    public Result addSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("新增套餐");
        setmealService.addSetmeal(setmealDTO);
        return Result.success();
    }
    //分页查询
    @GetMapping("/page")
    public Result selectPage(SetmealPageQueryDTO setmealPageQueryDTO){
        log.info("分页查询");
      PageResult pageResult = setmealService.selectPage(setmealPageQueryDTO);
       return Result.success(pageResult);
    }
    //批量删除套餐
    @DeleteMapping
    public Result deleteByIds(@RequestParam List<Long> ids){
            setmealService.deleteByIds(ids);
            return Result.success();

    }
   //id查询套餐
    @GetMapping("/{id}")
    public  Result selectById(@PathVariable Long id){
        log.info("根据id查询");
       SetmealVO setmealVO= setmealService.selectById(id);
       return Result.success(setmealVO);
    }
    //修改套餐
    @PutMapping
    public Result update(@RequestBody SetmealDTO setmealDTO){
        log.info("修改套餐");
        setmealService.update(setmealDTO);
        return Result.success();
    }
    //套餐启停操作
    @PutMapping("/status/{status}/{id}")
    public  Result updateStatus(@PathVariable Integer status,@PathVariable Long id){
        log.info("修改套餐状态");
        setmealService.updateStatus(status,id);
        return Result.success();
    }
}
