package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.service.WorkSpaceServer;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 19:38
 * @Description:
 */
@RestController
@RequestMapping("/admin/workspace")
@Slf4j
public class WorkSpaceController {
    @Autowired
    private WorkSpaceServer workSpaceServer;
    //查询今日运营数据
    @GetMapping("/businessData")
    public Result selectInfo(){
        log.info("开始查询运营数据");
        BusinessDataVO businessDataVO=  workSpaceServer.selectInfo();
        return Result.success(businessDataVO);
    }
    //查询今日订单数据
    @GetMapping("/overviewOrders")
    public Result todayOrderInfo(){
        log.info("开始查询今日的订单数据");
        OrderOverViewVO orderOverViewVO = workSpaceServer.todayOrderInfo();
        return Result.success(orderOverViewVO);
    }
    //查看菜品总览
    @GetMapping("/overviewDishes")
    public Result selectDishAll(){
        log.info("查看菜品的总览");
        DishOverViewVO dish =  workSpaceServer.selectDishAll();
         return Result.success(dish);
    }
    //查询套餐总览
    @GetMapping("//overviewSetmeals")
    public Result selectSetmealAll(){
        log.info("查询套餐总览");
        SetmealOverViewVO setmealOverViewVO =    workSpaceServer.selectSetmealAll();
        return Result.success(setmealOverViewVO);
    }
}
