package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 16:06
 * @Description:
 */
@RestController
@Slf4j
@RequestMapping("/admin/dish")
public class DishController {
    @Autowired
    private DishService dishService;
    //新增菜品
    @PostMapping
    public Result addDish(@RequestBody DishDTO dishDTO){
      dishService.addDish(dishDTO);
      return Result.success();
    }
    //分页查询
    @GetMapping("page")
    public Result<PageResult> pageList(DishPageQueryDTO dto){
      PageResult pageResult= dishService.pageList(dto);
      return Result.success(pageResult);
    }
    //批量删除菜品
    @DeleteMapping
    public Result delete(@RequestParam List<Integer> ids){
         dishService.delete(ids);
         return Result.success();
    }
    //根据id查询菜品
    @GetMapping("/{id}")
    public Result selectById(@PathVariable Long id){
     DishVO dishVO= dishService.selectById(id);
     return Result.success(dishVO);
    }
    //修改菜品
    @PutMapping
    public Result update(@RequestBody DishDTO dishDTO){
        log.info("修改菜品");
        dishService.update(dishDTO);
        return Result.success();
    }
    //起售停售功能开发
    @PutMapping("/status/{status}/{id}")
    public Result dishStatus(@PathVariable Integer status,@PathVariable Long id){
        log.info("停售起售功能");
        dishService.dishStatus(status,id);
        return Result.success();
    }
    //菜品列表查询
    @GetMapping("/list")
    public Result list(Integer categoryId,String name){
        log.info("套餐列表查询{}{}",categoryId,name);
        List<Dish>   dishes= dishService.list(categoryId,name);
         return Result.success(dishes);
    }

}
