package com.sky.controller.admin;

import com.github.pagehelper.PageHelper;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/21 18:03
 * @Description:
 */
@RestController
@RequestMapping("/admin/category")
@Slf4j
public class CategoryController  {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("page")
    public Result list(CategoryPageQueryDTO categoryPageQueryDTO){
        log.info("进行查询");
        PageResult pageResult= categoryService.list(categoryPageQueryDTO);
          return Result.success(pageResult);
    }
    //根据类型查询分类
    @GetMapping("/list")
    public Result selectByType(Integer type){
        List<Category>  categoryList=categoryService.selectByType(type);
        return Result.success(categoryList);
    }
    //根据id查询分类
    @GetMapping("/{id}")
    public  Result selectById(@PathVariable Long id){
      log.info("根据id查询{id}",id);
      Category category= categoryService.selectById(id);
      return Result.success(category);
    }
    //修改分类接口
    @PutMapping
    public Result update(@RequestBody CategoryDTO categoryDTO ){
        categoryService.update(categoryDTO);
        return Result.success();
    }
    //修改分类状态
    @PutMapping("/status/{status}/{id}")
    public Result categoryStatus(@PathVariable Integer status ,@PathVariable Long id){
        log.info("修改状态");
        categoryService.categoryStatus(status,id);
        return Result.success();
    }
    //新增分类
    @PostMapping
    public Result add(@RequestBody CategoryDTO categoryDTO){
      log.info("新增分类{}",categoryDTO);
      categoryService.add(categoryDTO);
      return Result.success();
    }
    //根据id删除分类
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id){
        log.info("删除分类");
        categoryService.delete(id);
        return Result.success();
    }



}
