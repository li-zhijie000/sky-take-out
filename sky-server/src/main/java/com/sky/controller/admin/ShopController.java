package com.sky.controller.admin;

import com.sky.constant.MessageConstant;
import com.sky.result.Result;
import com.sky.service.ShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 15:12
 * @Description:
 */
@RestController("adminShopcontroller")
@Slf4j
@RequestMapping("/admin/shop")
public class ShopController {
   @Autowired
   private ShopService shopService;

    @GetMapping("/status")
    public Result getStatus(){
        log.info("获取店铺营业状态");
        Integer status = shopService.getStatus();
        return Result.success(status);
    }
    //更改店铺营业状态
    @PutMapping("/{status}")
    public Result updateStatus(@PathVariable Integer status){
        log.info("更改店铺状态");
          shopService.setStatus(status);
          return Result.success();
    }

}
