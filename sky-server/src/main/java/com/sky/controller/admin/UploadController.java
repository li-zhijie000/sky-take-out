package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 15:09
 * @Description:
 */
@RestController
@Slf4j
public class UploadController {
    @Autowired
    private AliOssUtil aliOssUtil;
    @PostMapping("/admin/common/upload")
    public Result<String> upload(MultipartFile file) throws Exception {
        //取得后缀名
        String originalFilename = file.getOriginalFilename();
        String random= UUID.randomUUID().toString();
        String exName = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileName= random+exName;
        String url = aliOssUtil.upload(file.getBytes(), fileName);
        return Result.success(url);
    }
}
