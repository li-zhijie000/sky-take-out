package com.sky.controller.admin;

import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersConfirmDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrdersService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrdersListVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/30 11:24
 * @Description:
 */
@RestController("adminOrdersController")
@Slf4j
@RequestMapping("/admin/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    //订单搜索功能
    @GetMapping("/conditionSearch")
    public Result list(OrdersPageQueryDTO ordersPageQueryDTO){
    log.info("进行订单的搜索功能");
    PageResult pageResult =ordersService.adminList(ordersPageQueryDTO);
    return Result.success(pageResult);
    }
    //查询订单的数量
    @GetMapping("/statistics")
    public Result count(){
        log.info("查询订单的数量");
        OrderStatisticsVO   orderStatisticsVO  = ordersService.count();
        return Result.success(orderStatisticsVO);
    }
    //查询订单详情
    @GetMapping("/details/{id}")
    public Result selectDetailsById(@PathVariable Long id){
      log.info("查询订单详情");
         OrdersListVo ordersListVo= ordersService.selectDetailsById(id);
         return Result.success(ordersListVo);
    }
    //接单操作
    @PutMapping("/confirm")
    public Result receivingOrders(@RequestBody OrdersConfirmDTO orders){
        log.info("接单操作");
        ordersService.receivingOrders(orders);
        return Result.success();
    }
    //拒单操作
    @PutMapping("/rejection")
    public Result refuseOrder(@RequestBody OrdersRejectionDTO ordersRejectionDTO){
        log.info("拒绝接单");
         ordersService.refuseOrder(ordersRejectionDTO);
     return Result.success();
    }
    //取消订单操作
    @PutMapping("/cancel")
    public Result cancelOrder(@RequestBody OrdersCancelDTO ordersCancelDTO){
        log.info("取消订单操作");
        ordersService.adminCancelOrder(ordersCancelDTO);
         return Result.success();
    }
    //派送订单
    @PutMapping("/delivery/{id}")
    public Result deliveryOrder(@PathVariable Long id){
        log.info("派送订单");
        ordersService.deliveryOrder(id);
        return Result.success();
    }
    //完成订单
    @PutMapping("complete/{id}")
    public Result completeOrder(@PathVariable Long id){
        log.info("完成订单");
        ordersService.completeOrder(id);
        return Result.success();
    }
}
