package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.UserShopCartService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 20:47
 * @Description:
 */
@RestController
@RequestMapping("/user/shoppingCart")
@Slf4j
@Api("C端购物车相关接口")
public class UserShopCartController {
    @Autowired
    private UserShopCartService userShopCartService;
    @PostMapping("/add")
    public Result addShopCart(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("添加购物车");
        userShopCartService.addShopCard(shoppingCartDTO);
        return Result.success();
    }
    //查询购物车
    @GetMapping("/list")
    public Result list(){
        log.info("查询购物车");
       List<ShoppingCart> shoppingCarts= userShopCartService.list();
        return Result.success(shoppingCarts);
    }
    //清空购物车
    @DeleteMapping("clean")
    public Result deleteAll(){
        log.info("清空购物车");
          userShopCartService.clean();
          return Result.success();
    }
    //数量减一
    @PostMapping("/sub")
    public Result subNumber(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("减少商品数量");
        userShopCartService.subNumber(shoppingCartDTO);
        return Result.success();
    }
}
