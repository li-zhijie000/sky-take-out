package com.sky.controller.user;

import com.alibaba.fastjson.JSON;
import com.sky.dto.OrdersDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrdersService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.vo.OrdersListVo;
import com.sky.web.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：lizhijie
 * @Date：2023/6/29 12:20
 * @Description:
 */
@Slf4j
@RestController("userOrdersController")
@RequestMapping("user/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private WebSocketServer webSocketServer;

    //提交订单信息
    @PostMapping("/submit")
    public Result submit(@RequestBody OrdersSubmitDTO submitDTO) throws Exception {
        log.info("开始提交订单信息");
       OrderSubmitVO orderSubmitVO= ordersService.submit(submitDTO);
       return Result.success(orderSubmitVO);
    }
    //查询历史订单
    @GetMapping("/historyOrders")
    public Result list(OrdersPageQueryDTO ordersPageQueryDTO){
        log.info("查询历史订单");
        PageResult ordersListVoList = ordersService.list(ordersPageQueryDTO);
return  Result.success(ordersListVoList);
    }
    //查询订单详情
    @GetMapping("/orderDetail/{id}")
    public Result selectById(@PathVariable Long id){
        //查询订单详情
        log.info("查询订单的详情");
      OrdersListVo ordersListVo=  ordersService.selectById(id);
      return Result.success(ordersListVo);
    }
    //取消订单
    @PutMapping("/cancel/{id}")
    public Result cancelOrder(@PathVariable Long id){
        log.info("取消订单");
        ordersService.cancelOrder(id);
        return Result.success();
    }
    //再来一车
    @PostMapping("repetition/{id}")
    public Result Repurchase(@PathVariable Long id){
        log.info("根据id再来一车");
        ordersService.Repurchase(id);
        return Result.success();
    }
    //催单提醒
   @GetMapping("/reminder/{id}")
    public Result Reminder(@PathVariable Long id ) throws IOException {
        log.info("客户催单了,订单号为:{}",id);
        //查询订单号
       OrdersListVo ordersListVo = ordersService.selectDetailsById(id);
       //开始对管理端客户发起提醒
       Map<String,Object> map = new HashMap<>();
       map.put("type",2);
       map.put("orderId",ordersListVo.getId());
       map.put("content",ordersListVo.getNumber());
       webSocketServer.sendMessage(JSON.toJSONString(map));
       return Result.success();
   }
}
