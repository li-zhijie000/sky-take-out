package com.sky.controller.user;

import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：lizhijie
 * @Date：2023/6/29 19:25
 * @Description:
 */
@Slf4j
@RestController
public class WXPayment {
    @PutMapping("/user/order/payment")
    public Result pay(){
      log.info("接受到微信支付信息");
      return Result.success("15535345");
    }
}
