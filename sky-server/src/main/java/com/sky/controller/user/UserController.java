package com.sky.controller.user;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.*;
import com.sky.properties.JwtProperties;
import com.sky.result.Result;
import com.sky.service.*;
import com.sky.utils.JwtUtil;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import com.sky.vo.UserLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 16:13
 * @Description:
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtProperties jwtProperties;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private DishService dishService;

    @PostMapping("/user/login")
    public Result userLogin(@RequestBody UserLoginDTO userLoginDTO) {
        log.info("开始登录");
        User user = userService.userLogin(userLoginDTO);
        //调用jwt令牌给他令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.USER_ID, user.getId());
        String jwt = JwtUtil.createJWT(jwtProperties.getUserSecretKey(), jwtProperties.getUserTtl(), claims);
        UserLoginVO build = UserLoginVO.builder().id(user.getId()).openid(user.getOpenid()).token(jwt).build();
        return Result.success(build);
    }

    //根据查询套餐列表
    @GetMapping("/category/list")
    public Result categoryList(Integer type) {
        log.info("根据套餐");
        List<Category> categoryList = categoryService.listByType(type);
        return Result.success(categoryList);
    }

    //根据分类id查询套餐列表
    @GetMapping("/setmeal/list")
    public Result setmealList(Long categoryId) {
        log.info("根据分类id");
        List<Setmeal> setmeals = setmealService.selectByCategoryId(categoryId);
        return Result.success(setmeals);
    }

    //根据分类id查询菜品
    @GetMapping("/dish/list")
    public Result dishList(Long categoryId) {
        log.info("根据分类id查询菜品");
        List<DishVO> dishVOList = dishService.selectByCategoryId(categoryId);
        return Result.success(dishVOList);
    }

    //根据套餐id查询菜品的数据
    @GetMapping("/setmeal/dish/{id}")
    public Result selectBySetmealId(@PathVariable Long id) {
        log.info("套餐id查询菜品的数据{}", id);
        List<DishItemVO> dishes = dishService.selectBySetmealId(id);
        return Result.success(dishes);
    }

}

