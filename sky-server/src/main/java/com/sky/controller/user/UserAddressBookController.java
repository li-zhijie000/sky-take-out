package com.sky.controller.user;

import com.sky.entity.AddressBook;
import com.sky.result.Result;
import com.sky.service.UserAddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/27 17:37
 * @Description:
 */
@RestController
@Slf4j
@RequestMapping("/user/addressBook")
public class UserAddressBookController {
    @Autowired
    private UserAddressBookService userAddressBookService;

    //新增收货地址
    @PostMapping
    public Result addAddress(@RequestBody AddressBook addressBook){
    userAddressBookService.add(addressBook);
    return Result.success();
    }
    //地址列表
    @GetMapping("/list")
    public Result addressList(){
        log.info("展示地址列表");
        List<AddressBook> addressBookList= userAddressBookService.list();
        return Result.success(addressBookList);
    }
    //查询回显
    @GetMapping("/{id}")
    public  Result selectById(@PathVariable Long id){
        log.info("根据id查询");
      AddressBook addressBook=  userAddressBookService.selectById(id);
       return Result.success(addressBook);
    }
    //修改地址
    @PutMapping
    public Result update(@RequestBody AddressBook addressBook){
        log.info("修改地址");
        userAddressBookService.update(addressBook);
        return Result.success();
    }
    //删除地址
    @DeleteMapping
    public  Result delete(Long id ){
        log.info("删除id为{}的地址",id);
        userAddressBookService.delete(id);
        return Result.success();
    }
    //设置默认地址
    @PutMapping("/default")
    public Result defaultAddress(@RequestBody AddressBook addressBook){
        log.info("设置默认地址");
        userAddressBookService.defaultAddress(addressBook);
        return Result.success();
    }
    //查询默认地址
    @GetMapping("/default")
    public Result selectDefaultAddress(){
        log.info("查询默认地址");
       AddressBook addressBook = userAddressBookService.selectDefaultAddress();
       return Result.success(addressBook);
    }
}
