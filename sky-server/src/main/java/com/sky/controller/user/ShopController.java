package com.sky.controller.user;

import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 16:00
 * @Description:
 */
@RestController("userShopcontroller")
@RequestMapping("/user/shop")
@Slf4j
public class ShopController {
    @Autowired
    private RedisTemplate redisTemplate;
    @GetMapping ("/status")
    public Result getStatus(){
    log.info("获取店铺登录状态");
        Object status = redisTemplate.opsForValue().get("status");
        return Result.success(status);
    }
}
