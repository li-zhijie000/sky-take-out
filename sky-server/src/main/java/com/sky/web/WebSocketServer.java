package com.sky.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@ServerEndpoint("/ws/{sid}")
public class WebSocketServer {
    //定义集合存储session对象
   private static Map<String, Session> map = new HashMap<>();
    @OnOpen
    public void open(Session session ,@PathParam("sid") String sid){
        log.info("连接成功sid是{}",sid);
        map.put(sid,session);
        log.info("添加连接个数为{}",map.size());
    }
    @OnMessage
    public void message(Session session ,@PathParam("sid") String sid,String message){
        log.info("客户端发送的消息是{}",message);
    }
    @OnClose
    public void  close(Session session ,@PathParam("sid") String sid){
        log.info("关闭连接");
        map.remove(sid);
    }

    //定义群发消息的方法
    public void sendMessage(String message) throws IOException {
        log.info("准备群发消息{}",message);
        Collection<Session> values = map.values();
        //对于session进行非空判断
        if (CollectionUtils.isEmpty(values)) {
            log.info("没有建立连接的session对象无法发送消息");
            return;
        }
        //开始进行消息的发送
        for (Session session : values) {
            session.getBasicRemote().sendText(message);
        }
    }
}