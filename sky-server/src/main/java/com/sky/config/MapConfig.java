package com.sky.config;

import com.sky.properties.BaiduMapProperties;
import com.sky.utils.MapUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：lizhijie
 * @Date：2023/7/2 19:56
 * @Description:
 */
@Configuration
public class MapConfig {
    //注入地图工具类
    @Bean
    public MapUtils mapUtils(BaiduMapProperties baiduMapProperties){
        return new MapUtils(baiduMapProperties);
    }
}
