package com.sky.interceptor;

import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author：lizhijie
 * @Date：2023/6/20 16:28
 * @Description:
 */
@Slf4j
@Component
public class AdminLoginInterceptor implements HandlerInterceptor {
    @Autowired
    private JwtProperties jwtProperties;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("拦截器拦截到了{}",request.getRequestURL());
        //开始配置拦截逻辑
        String token = request.getHeader(jwtProperties.getAdminTokenName());
        if (!StringUtils.hasLength(token)){
            log.info("令牌为空");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }
        //校验令牌
        try {
            Claims claims = JwtUtil.parseJWT(jwtProperties.getAdminSecretKey(), token);
            Object o = claims.get(JwtClaimsConstant.EMP_ID);
            BaseContext.setCurrentId(Long.valueOf(o.toString()));
        } catch (Exception e) {
            log.info("令牌无效");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }
        return true;

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        BaseContext.removeCurrentId();
    }
}
