package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 20:52
 * @Description:
 */
@Mapper
public interface UserShopCartMapper {
    void add(ShoppingCart shoppingCart);

    List<ShoppingCart> selectByUserId(ShoppingCart shoppingCart);

    void update(ShoppingCart cart);

    @Delete("delete from shopping_cart where user_id=#{currentId}")
    void deleteAllById(Long currentId);

    ShoppingCart selectNumberByUserId(ShoppingCart shoppingCart);

    void deleteById(ShoppingCart shoppingCart);
}
