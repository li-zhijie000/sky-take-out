package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/27 19:22
 * @Description:
 */
@Mapper
public interface UserAddressBookMapper {

    void add(AddressBook addressBook);

    @Select("select id, user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label, is_default, create_time from address_book ")
    List<AddressBook> list();

    AddressBook selectById(AddressBook addressBook);

    void update(AddressBook addressBook);

    @Delete("delete from address_book where id = #{id}")
    void delete(Long id);
}
