package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 17:39
 * @Description:
 */
@Mapper
public interface DishFlavorMapper {
    void add(List<DishFlavor> flavors);

    void delete(List<Integer> ids);

    @Select("select id, dish_id, name, value from dish_flavor where dish_id=#{id}")
    List<DishFlavor> selectByDishId(Long id);
}
