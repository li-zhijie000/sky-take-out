package com.sky.mapper;

import com.sky.entity.SetmealDish;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 20:06
 * @Description:
 */
@Mapper
public interface DishAndSetmealMapper {
    List<Long> selectSetmealByDishid(List<Integer> ids);

    void add(List<SetmealDish> setmealDishes);

    void delete(List<Long> ids);

    List<SetmealDish> selectBySetmeal(Long id);

    @Select("select count(*) from setmeal_dish sd left join dish d on sd.dish_id = d.id where setmeal_id=#{id} and d.status=0 ")
    Long selectStatusBySetmeal(Long id);
}
