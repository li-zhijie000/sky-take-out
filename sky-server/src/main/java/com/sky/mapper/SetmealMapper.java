package com.sky.mapper;

import com.sky.annotation.AutoFillAnnotation;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealOverViewVO;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/22 1:42
 * @Description:
 */
@Mapper
public interface SetmealMapper {
    @Select("select id, name, category_id, price, status, description, image, create_time, update_time, create_user, update_user from setmeal where category_id=#{id}")
    List<Setmeal> selectByCategoryId(Long id);

     @AutoFillAnnotation(OperationType.UPDATE)
    void updateStatus(Setmeal setmeal);


    @AutoFillAnnotation(OperationType.INSERT)
    void addSetmeal(Setmeal setmeal);

    List<SetmealVO> selectPage(SetmealVO setmeal);

    Long selectStatus(List<Long> ids);

    void delete(List<Long> ids);

    @Select("select id, name, category_id, price, status, description, image, create_time, update_time, create_user, update_user from setmeal where id=#{id}")
    Setmeal selectById(Long id);

    @Select("select sum(if(status=1,1,0)) 'sold' ,sum(if(status=0,1,0))'discontinued' from setmeal")
    SetmealOverViewVO selectSetmealAll();
}
