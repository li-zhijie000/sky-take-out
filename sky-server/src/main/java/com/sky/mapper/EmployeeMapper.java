package com.sky.mapper;

import com.sky.annotation.AutoFillAnnotation;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import com.sky.vo.EmployeeLoginVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    @Select("select id, name, username, password, phone, sex, id_number, status, create_time, update_time, create_user, update_user from employee where username=#{username}  ")
    Employee employeeLogin(String username);

@AutoFillAnnotation(OperationType.INSERT)
    void addEmployee(Employee employee);

    List<Employee> list(String name);
@AutoFillAnnotation(OperationType.UPDATE)
    void update(Employee employee);

    @Select("select id, name, username, password, phone, sex, id_number, status, create_time, update_time, create_user, update_user from employee where id=#{id}")
    Employee employeeById(Long id);
}
