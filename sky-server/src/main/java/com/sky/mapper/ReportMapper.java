package com.sky.mapper;

import com.sky.dto.TurnoverReportDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/7/3 11:22
 * @Description:
 */
@Mapper
public interface ReportMapper {
    @Select("select *from orders where order_time between #{begin} and #{end} and status=#{status}")
    List<TurnoverReportDTO> selectTurnover(LocalDate begin, LocalDate end, Integer status);
}
