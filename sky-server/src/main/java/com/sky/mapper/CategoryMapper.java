package com.sky.mapper;

import com.sky.annotation.AutoFillAnnotation;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/21 18:06
 * @Description:
 */
@Mapper
public interface CategoryMapper {

    List<Category> list(String name, Integer type);

    @Select("select id, type, name, sort, status, create_time, update_time, create_user, update_user from category where id=#{id}")
    Category selectById(Long id);
    @AutoFillAnnotation(OperationType.UPDATE)
    void update(Category category);
   @AutoFillAnnotation(OperationType.INSERT)
    void add(Category category);

    @Delete("delete from category where id=#{id}")
    void delete(Long id);

    @Select("select id, type, name, sort, status, create_time, update_time, create_user, update_user from category where type=#{type}")
    List<Category> selectByType(Integer type);

    List<Category> listByType(Integer type);
}
