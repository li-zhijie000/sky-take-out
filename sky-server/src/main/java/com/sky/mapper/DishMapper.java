package com.sky.mapper;

import com.sky.annotation.AutoFillAnnotation;
import com.sky.entity.Dish;
import com.sky.entity.SetmealDish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/22 1:42
 * @Description:
 */
@Mapper
public interface DishMapper {

    List<Dish> selectByCategoryId(Long id,String name);

    @AutoFillAnnotation(OperationType.INSERT)
    @Options(useGeneratedKeys = true,keyProperty ="id")
    @Insert("insert into dish ( name, category_id, price, image, description, status, create_time, update_time, create_user, update_user) VALUES(#{name},#{categoryId},#{price},#{image},#{description},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void addDish(Dish dish);


    List<DishVO> pageList(DishVO dishVO);

    Long selectCountById(List<Integer> ids);

    void delete(List<Integer> ids);

    @Select("select id, name, category_id, price, image, description, status, create_time, update_time, create_user, update_user from dish where id=#{id}")
    Dish selectById(Long id);

    @AutoFillAnnotation(OperationType.UPDATE)
    void update(Dish dish);

    List<DishItemVO> selectBySetmealId(Long id);

    DishOverViewVO selectByStatus();
}
