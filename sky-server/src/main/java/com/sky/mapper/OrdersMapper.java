package com.sky.mapper;

import com.sky.dto.*;
import com.sky.entity.Orders;
import com.sky.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @Author：lizhijie
 * @Date：2023/6/29 12:26
 * @Description:
 */
@Mapper
public interface OrdersMapper {
    void add(Orders orders);


    List<OrdersListVo> list(Integer status, Long currentId);

    OrdersListVo selectById(Orders orders);

    Long selectStatusById(Orders orders);

    void updateStatus(Orders orders);

    List<OrdersListVo> selectByCondition(OrdersPageQueryDTO orders);

    List<Map<String, Object>> selectCount();

    Orders selectOrderById(OrdersConfirmDTO orders);

    @Select("select id, number, status, user_id, address_book_id, order_time, checkout_time, pay_method, pay_status, amount, remark, phone, address, consignee, user_name, cancel_reason, rejection_reason, cancel_time, estimated_delivery_time, delivery_status, delivery_time, pack_amount, tableware_number, tableware_status from orders where order_time<#{beforeTime} and status=#{status}")
    List<Orders> selectOverOrders(Integer status, LocalDateTime beforeTime);

    List<TurnoverReportDTO> selectTurnover(LocalDateTime begin, LocalDateTime end, Integer orderStautsCompleted);

    List<OrderReportDTO> selectCountByDate(LocalDateTime begin, LocalDateTime end,Integer status);

    List<SalesReportDTO> selectSalesTop10(LocalDateTime begin, LocalDateTime end, Integer status);


    BusinessDataVO selectInfo(LocalDateTime today);

    List<Map<String,Object>> selectTodayOrder(LocalDateTime min, LocalDateTime max);

    BusinessDataVO selectDataByDate(LocalDateTime begin, LocalDateTime end);
}
