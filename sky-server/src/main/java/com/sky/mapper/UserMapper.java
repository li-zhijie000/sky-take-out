package com.sky.mapper;

import com.sky.dto.UserReportDTO;
import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/26 16:15
 * @Description:
 */
@Mapper
public interface UserMapper {

    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where openid=#{openid}")
    User userLogin(String openid);
    @Options(keyProperty = "id",useGeneratedKeys = true)
    @Insert("insert into user ( openid, create_time)values (#{openid},#{createTime})")
    void addUser(User user);

    List<UserReportDTO> selectAddCount(LocalDateTime begin, LocalDateTime end);

    @Select("select count(*) from user where create_time<#{begin}")
    Integer selectBaseCountByDate(LocalDateTime begin);
}
