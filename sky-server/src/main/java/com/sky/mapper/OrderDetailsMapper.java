package com.sky.mapper;

import com.sky.entity.OrderDetail;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author：lizhijie
 * @Date：2023/6/29 15:04
 * @Description:
 */
@Mapper
public interface OrderDetailsMapper {
       void add(List<OrderDetail> orderDetails) ;

    List<OrderDetail> selectByNumber(String number);
}
