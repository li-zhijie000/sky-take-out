package com.sky.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：lizhijie
 * @Date：2023/6/30 21:38
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressAnalysis {
    private Location location;
    private String precise;
    private String confidence;
    private String comprehension;
    private String level;
}
