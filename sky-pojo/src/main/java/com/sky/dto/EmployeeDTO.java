package com.sky.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("前端数据传输封装类")
public class EmployeeDTO implements Serializable {
   @ApiModelProperty("唯一标识")
    private Long id;
@ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("名字")
    private String name;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("性别")
    private Integer sex;
    @ApiModelProperty("身份证号")
    private String idNumber;

}
